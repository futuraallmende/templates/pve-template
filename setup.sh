#!/usr/bin/env bash
pip install ansible netaddr proxmoxer uptime_kuma_api passlib
ansible-galaxy collection install -r roles/requirements.yml -U
ansible-galaxy role install -r roles/requirements.yml
ssh-add -D
ssh-add ~/.ssh/cloud-ovh
ssh-add
